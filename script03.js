let n = prompt('Введите любое натуральное число: ', '');
while(Boolean(n) == false) {
    n = prompt("Incorrect! Please, enter correct number (integer, greater then 1):", [n]);
}
while((n % 1) !=0) {
    n = prompt("Incorrect! Please, enter correct number (integer, greater then 1):", [n]);
}
while((n > 1) == false) {
    n = prompt("Incorrect! Please, enter correct number (integer, greater then 1):", [n]);
}
while((n < 300) == false) {
    n = prompt("Ну уж нет - не более 300! Please, enter correct number (integer, greater then 1):", [n]);
}

let result;

function facto(x) {
  if(x != 2) {
      return(x * facto(x - 1));
  } else {
      return(2);
  }
}

result = facto(n);

// А это вариант подсчета через цикл:

// function factor(x) {
//     let result = x;
//     for (let i = 1; i < n; ++i) {
//       result *= i;
//     }
//     return(result);
// }

// result = factor(n);

document.write('Факториал числа ' + n + ' равен: ' + result);


